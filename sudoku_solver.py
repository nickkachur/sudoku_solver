"""Solve simple Sudoku puzzles."""


import collections
import math
import sys


Coordinate = collections.namedtuple("Coordinate", "x y")
Coordinate.__doc__ = "Used as a convenience return type."


class SudokuPuzzle:
    """Represent a 9x9 Sudoku puzzle.

    Representation (as 2 x 2):
    <-- x -->
    +---+---+  ^
    | a | b |  |
    +---+---+  y
    | a | b |  |
    +---+---+  v
    """

    def __init__(self, rows, name="Sudoku Puzzle"):
        self.order = 9
        self.square_size = int(math.sqrt(self.order))
        if len(rows) < self.order or any([len(row) < self.order for row in rows]):
            raise ValueError(
                "Input rows and columns must all have order {}".format(self.order)
            )
        self.rows = rows
        self.name = name

    def pretty_print(self):
        row_delimiter = "+" + ("---+" * self.order)
        print(row_delimiter)
        for row in self.rows:
            row_string = "| " + " | ".join([str(cell) for cell in row]) + " |"
            print(row_string)
            print(row_delimiter)

    def get_number_by_cell_coordinates(self, cell_x, cell_y):
        """Get the number in a cell by its coordinates.

        A helper method so as not to have to repeat this calculation
        and potentially do it wrong.
        """
        return self.rows[cell_y][cell_x]

    def set_number_by_cell_coordinates(self, cell_x, cell_y, number):
        """Set the number in a cell by its coordinates.

        A helper method so as not to have to repeat this calculation
        and potentially do it wrong.
        """
        self.rows[cell_y][cell_x] = number

    def get_all_numbers_in_row(self, row_y):
        return self.rows[row_y]

    def get_all_numbers_in_column(self, column_x):
        return [row[column_x] for row in self.rows]

    def get_square_coordinates_for_cell_coordinates(self, cell_x, cell_y):
        square_x = cell_x // self.square_size
        square_y = cell_y // self.square_size
        return Coordinate(x=square_x, y=square_y)

    def get_all_numbers_in_square_by_cell_coordinates(
        self, original_cell_x, original_cell_y
    ):
        square_coordinates = self.get_square_coordinates_for_cell_coordinates(
            original_cell_x, original_cell_y
        )
        cell_x_start = square_coordinates.x * self.square_size
        cell_y_start = square_coordinates.y * self.square_size
        all_numbers = []
        for cell_y in range(cell_y_start, cell_y_start + self.square_size):
            for cell_x in range(cell_x_start, cell_x_start + self.square_size):
                all_numbers.append(self.get_number_by_cell_coordinates(cell_x, cell_y))
        return all_numbers

    def is_possible_number_for_cell(self, cell_x, cell_y, number):
        is_number_taken = any(
            [
                number in self.get_all_numbers_in_row(cell_y),
                number in self.get_all_numbers_in_column(cell_x),
                number
                in self.get_all_numbers_in_square_by_cell_coordinates(cell_x, cell_y),
            ]
        )
        return not is_number_taken

    def naive_solve(self):
        """Naive, recursive, backtracking in-place solve."""
        for column_x in range(self.order):
            for row_y in range(self.order):
                if self.get_number_by_cell_coordinates(column_x, row_y) == 0:
                    for number in range(1, self.order + 1):
                        if self.is_possible_number_for_cell(column_x, row_y, number):
                            self.set_number_by_cell_coordinates(column_x, row_y, number)
                            self.naive_solve()
                            self.set_number_by_cell_coordinates(column_x, row_y, 0)
                    return
        print("naive_solve solution:")
        self.pretty_print()


EMPTY_PUZZLE = SudokuPuzzle(
    [
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]
)


"""Sample puzzles from:
https://dingo.sbs.arizona.edu/~sandiway/sudoku/examples.html
"""
EASY_PUZZLE = SudokuPuzzle(
    [
        [0, 0, 0, 2, 6, 0, 7, 0, 1],
        [6, 8, 0, 0, 7, 0, 0, 9, 0],
        [1, 9, 0, 0, 0, 4, 5, 0, 0],
        [8, 2, 0, 1, 0, 0, 0, 4, 0],
        [0, 0, 4, 6, 0, 2, 9, 0, 0],
        [0, 5, 0, 0, 0, 3, 0, 2, 8],
        [0, 0, 9, 3, 0, 0, 0, 7, 4],
        [0, 4, 0, 0, 5, 0, 0, 3, 6],
        [7, 0, 3, 0, 1, 8, 0, 0, 0],
    ]
)


def main():
    ep = EASY_PUZZLE
    print("\n\nsudoku_solver.py: Easy Puzzle initial state:")
    ep.pretty_print()
    print("\n\n")
    ep.naive_solve()


if __name__ == "__main__":
    main()
    sys.exit(0)
